<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rate the Crew</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
     <?php include 'includes/styles.php'?>
     <?php include 'includes/arrayObjects.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main class="subPage">       
        <!-- sub page body -->
        <div class="subpageBody">
            <div class="container">
                <div class="row justify-content-center">

                <div class="col-md-8 col-lg-5">
                    <div class="signinCol">
                        <article>
                            <h3 class="fbold">Sign in</h3>
                            <p>Sign in with your data that you entered during your registration</p>
                        </article>

                        <form class="form pt-3" method="">
                            <div class="form-group">
                                <label>Username</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Password</label>
                                <div class="input-group">
                                    <input type="password" class="form-control" name="">
                                </div>
                            </div>

                            <div class="d-flex justify-content-between">
                                <div>
                                    <input type="checkbox"><small class="d-inline-block ps-1">Keep me logged in</small>
                                </div>
                                <a href="resetpassword.php" class="fred">Forgot Password</a>
                            </div>
                            
                            <input onclick="window.location.href='customerProfile.php';" type="button" class="redlink w-100 mt-3" value="Sign in">
                            <p class="text-center py-2">Dont have an Account?</p>

                            <div class="d-flex justify-content-between">
                                <p>Restaurant <a href="businesssignup.php" class="fred"> Signup</a></p>
                                <p>Crew <a href="crewRegistration.php" class="fred"> Signup</a></p>
                            </div>

                            <div>
                                <a href="javascript:void(0)" class="redbrdlink d-block text-center fblack">
                                    <img src="img/Google-Button.png" class="pe-2">Sign with Google</a>
                            </div>
                        </form>
                    </div>
                </div>   
                </div>
            </div>
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>