<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rate the Crew</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
     <?php include 'includes/styles.php'?>
     <?php include 'includes/arrayObjects.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main>
        <!-- slider -->
        <div class="homeSlider">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <img src="img/bannerimg.png" alt="" class="img-fluid">
                    </div>
                    <div class="col-md-6 align-self-center text-center">
                        <h1 class="fwhite fbold text-center pb-3">Become Top Rated Crew in Top Restaurants</h1>
                        <a href="javascript:void(0)" class="translinkUpdated fbold">Nominate Crew</a>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sider ends -->
        <!-- top rated restaurants-->
        <section class="section">
            <!-- container -->
            <div class="customContainer">
               <div class="d-flex justify-content-between">
                    <h2 class="sectionTitle"> Top Rated Restaurants </h2>
                    <a href="javascript:void(0)" class="redlink">View All</a>
               </div>
                <!-- tab starts -->
                <div class="TopRestaurantsTab">
                    <div id="parentHorizontalTab">
                        <ul class="resp-tabs-list hor_1">
                            <li>All</li>
                            <li>Featured</li>
                            <li>Best Rate</li>
                            <li>Most View</li>
                            <li>Popular</li>
                        </ul>
                        <div class="resp-tabs-container hor_1">
                            <div>
                                <!-- list -->
                                <div class="listRest">
                                    <div class="row">
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest01img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Raman Kraft Hotel</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest03img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Flavor Town</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest02img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Big Bites</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest01img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Choice Foods</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                    </div>
                                </div>
                                <!-- list -->
                            </div>
                            <div>
                                <!-- features -->
                                <!-- list -->
                                <div class="listRest">
                                    <div class="row">
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest01img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Raman Kraft Hotel</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest02img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Flavor Town</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest03img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Big Bites</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest04img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Choice Foods</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                    </div>
                                </div>
                                <!-- list -->
                                <!--/ featured-->
                            </div>
                            <div>
                                <!-- best rate-->
                                <!-- list -->
                                <div class="listRest">
                                    <div class="row">
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest04img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Raman Kraft Hotel</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest03img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Flavor Town</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest02img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Big Bites</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest01img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Choice Foods</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                    </div>
                                </div>
                                <!-- list -->
                                <!--/ best rate-->
                            </div>
                            <div>
                                <!--  Most view -->
                                <!-- list -->
                                <div class="listRest">
                                    <div class="row">
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest01img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Raman Kraft Hotel</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest02img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Flavor Town</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest03img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Big Bites</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest04img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Choice Foods</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                    </div>
                                </div>
                                <!-- list -->
                                <!--/ Most view -->
                            </div>
                            <div>
                                <!-- popular -->
                                <!-- list -->
                                <div class="listRest">
                                    <div class="row">
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest04img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Raman Kraft Hotel</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest03img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Flavor Town</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest02img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Big Bites</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest01img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Choice Foods</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                    </div>
                                </div>
                                <!-- list -->
                                <!--/ popular -->
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ tab ends -->
            </div>
            <!--/ container -->
        </section>
        <!--/ top rated restaurants -->
        <!-- top rated crews -->
        <section class="section topRatedcrews">
            <!-- container custom -->
            <div class="customContainer">
                <div class="d-flex justify-content-between py-4">
                    <h2 class="sectionTitle"> Top Rated Crews </h2>
                    <a href="javascript:void(0)" class="redlink">View All Crews</a>
               </div>               
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <?php 
                    for($i=0; $i<count ($crewItem); $i++){?>
                    <div class="col-md-4 col-lg-2 text-center crewcol">
                        <figure>
                            <a href="javascript:void(0)">
                                <img src="img/crews/<?php echo $crewItem [$i][0]?>.jpg" alt="" class="img-fluid">
                            </a>
                        </figure>
                        <article>
                            <a class="fbold h5" href="javascript:void(0)"><?php echo $crewItem [$i][1]?></a>
                            <p><small><?php echo $crewItem [$i][2]?></small></p>
                            <p class="border-top pt-3"><span class="customerRate px-2 white me-2"><?php echo $crewItem [$i][3]?></span>Customer
                                Rating </p>
                        </article>
                    </div>
                    <?php } ?>
                    <!--/ col -->                 
                </div>
                <!--/ row -->
            </div>
            <!--/ container custom-->
        </section>
        <!--/ top reated crews -->
        <!-- nominated Crew -->
        <section class="section nominatedCrew">
            <div class="container">
                <div class="py-4">
                    <h2 class="sectionTitle text-center"> Nominate Crew </h2>
                </div>
                <form>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Business/Store Name</label>
                                <div class="input-group">
                                    <select class="form-control">
                                        <option>Select</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Select Crew Name</label>
                                <div class="input-group">
                                    <select class="form-control">
                                        <option>Select</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Happy with Service?</label>
                                <div class="input-group">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions"
                                            id="inlineRadio1" value="option1">
                                        <label class="form-check-label" for="inlineRadio1">Yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions"
                                            id="inlineRadio2" value="option2">
                                        <label class="form-check-label" for="inlineRadio2">No</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Write Feedback</label>
                                <div class="input-group">
                                    <textarea class="form-control" placeholder="Write Feedback"
                                        style="height:100px;"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Do you want to tip him?</label>
                                <div class="input-group">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions"
                                            id="inlineRadio1" value="option1">
                                        <label class="form-check-label" for="inlineRadio1">Yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions"
                                            id="inlineRadio2" value="option2">
                                        <label class="form-check-label" for="inlineRadio2">No</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-5 col-md-2">
                            <div class="form-group">
                                <label>Tip Amount</label>
                                <div class="input-group">
                                    <input type="text" placeholder="$ 0.00" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-7 col-md-2">
                            <button class="redlink mt-4">Make Payment</button>
                        </div>
                        <div class="col-md-12">
                            <button class="redlink">Submit</button>
                            <button class="redbrdlink"> Cancel </button>
                        </div>
                    </div>
            </div>
            </form>
            </div>
        </section>
        <!--/ Nomiated crew -->
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>