<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rate the Crew</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
     <?php include 'includes/styles.php'?>
     <?php include 'includes/arrayObjects.php'?>
</head>

<body>
    <?php include 'includes/headerPostlogin.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
       <div class="container profilePage">
           <h2 class="h4 fbold pb-3">Notifications</h2>

           <div class="d-sm-flex justify-content-between">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="customerProfile.php">Username</a></li>
                        <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
                        <li class="breadcrumb-item active" aria-current="page">Notifications</li>
                    </ol>
                </nav>
                <p class="fgray text-right">Last Updated on : <span class="fblack fsbold">31 May 2021</span></p>
           </div>
            
       </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpageBody postLoginPage">
             <!-- container -->
            <div class="container">  
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-md-2">
                       <?Php include 'includes/userLeftNav.php'?>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-10">
                        <!-- right profile -->
                        <div class="rightProfile composemsg">
                              <h4 class="pb-3">My Kids Lost Items </h4>
                              <!-- mail write-->
                               <div class="mailwrote p-md-3 p-2 border mb-2">
                                    <div class="row border-bottom">
                                        <div class="col-md-6">
                                            <p class="pb-0 mb-0">From: <span><b>Praveen Guptha</b></span></p>
                                            <p>To: <span><b>Venkat Siri Innovations</b></span></p>
                                        </div>
                                        <div class="col-md-6 text-end">
                                            <p><small>8th, Ap 2022, 01:35PM</small></p>
                                        </div>                                       
                                    </div>
                                   
                                    <article class="py-3">
                                        <p>Hi Principal</p>
                                        <p class="pb-0 mb-0">Today, My Kid (Vimudha Valli RT 3C) forgot her swim goggles at School (swimming area)Could you please forward this request for find out her goggles, she will collect on monday</p>
                                    </article>
                                    <article>
                                        <p>Thanks & Regards</p>
                                        <p class="pb-0 mb-0">Balakrishna T</p>
                                        <p>9652042202</p>
                                    </article>                                       
                               </div>
                              <!--/ mail write -->

                             <!-- mail write-->
                             <div class="mailwrote p-md-3 p-2 border mb-2">
                                    <div class="row border-bottom">
                                        <div class="col-md-6">
                                            <p class="pb-0 mb-0"><span><b>1099017254 (Ishaan Sripathi) - PP2 - Section A</b></span></p>
                                            
                                        </div>
                                        <div class="col-md-6 text-end">
                                            <p><small>8th, Ap 2022, 01:35PM</small></p>
                                        </div>                                       
                                    </div>
                                   
                                    <article class="py-3">
                                        <p>Hi Principal</p>
                                        <p class="pb-0 mb-0">Today, My Kid (Vimudha Valli RT 3C) forgot her swim goggles at School (swimming area)Could you please forward this request for find out her goggles, she will collect on monday</p>
                                    </article>                                                                    
                               </div>
                              <!--/ mail write -->

                               <!-- mail write-->
                             <div class="mailwrote p-md-3 p-2 border mb-2">
                                    <div class="row border-bottom">
                                        <div class="col-md-6">
                                            <p class="pb-0 mb-0"><span><b>1099017254 (Ishaan Sripathi) - PP2 - Section A</b></span></p>
                                            
                                        </div>
                                        <div class="col-md-6 text-end">
                                            <p><small>8th, Ap 2022, 01:35PM</small></p>
                                        </div>                                       
                                    </div>
                                   
                                    <article class="py-3">
                                        <p>Hi Principal</p>
                                        <p class="pb-0 mb-0">Today, My Kid (Vimudha Valli RT 3C) forgot her swim goggles at School (swimming area)Could you please forward this request for find out her goggles, she will collect on monday</p>
                                    </article>                                                                    
                               </div>
                              <!--/ mail write -->


                            
                              <!-- text editor-->
                              <div class="text-editor">
                                <div class="editor-full">
                                        <div id="document-full" class="ql-scroll-y" style="height: 300px;">                                          
                                        </div>
                                        <div class="form-group py-2">
                                            <label>Upload Attachment</label>
                                            <input type="file" class="form-control w-100 d-block">
                                        </div>                                        
                                        <button type="button" class="btn btn-primary">Submit</button>
                                </div> 
                              </div>
                              <!--/ text editor -->
                        </div>
                        <!--/ right profile -->
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
              
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->

     
      
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>