<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rate the Crew</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
     <?php include 'includes/styles.php'?>
     <?php include 'includes/arrayObjects.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
       <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
                    <li class="breadcrumb-item active" aria-current="page">Terms &amp; Conditions</li>
                </ol>
            </nav>
       </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpageBody">
            <div class="container">
                <div class="row">

                <div class="col-md-6">
                    <h2>Terms & Conditions</h2>
                    <h5 class="fsbold">Introduction</h5>
                    <p>Welcome to Reward That Crew. By using our website and/or using the services that are provided, you acknowledge that you have read, understand, and agree to be bound by our Terms and Conditions. These Terms and Conditions unconditionally extend and apply to all related applications, internet services, or website extensions. If you are not in agreement with all these Terms and Conditions, you are prohibited from using Reward That Crew and you may discontinue use immediately. Reward That Crew recommends that you save and/or print a copy of these Terms and Conditions for future reference. </p>
                     <h5 class="fsbold">Agreement to Terms and Conditions: </h5>
                    <p>Reward That Crew Terms and Conditions (these “Terms” or these “Terms and Conditions”) contained in this agreement shall govern your use of this Website and all of its content (collectively referred to herein as this “Website”) These terms outline the rules and regulations guiding the use of Reward That Crew located at <a href="index.php" class="fsbold">www.rewardthatcrew.com</a>. All material, information, documents services or all other entities (collectively referred to as content) that appear on Reward That Crew shall be administered subject to  these Terms and Conditions, and the use of this website constitutes an express agreement with all the terms and conditions contained herein in full. Do not continue to use this website if you have any objections to any of the Terms and Conditions stated on this page. </p>
                </div>
                <div class="col-md-6">
                     <h5 class="fsbold">Definitions/Terminology </h5>
                     <p>The following definitions apply to these Terms and Conditions, Privacy Statements, Disclaimer Notice and all Agreements. “Users”, “Visitors”, “Client”, “Customer”, “You” and “Your” refers to you, the person(s) that use the website. Reward That Crew, “We”, “Our” and “Us”, refers to our website and Reward That Crew. “Party” or “Parties”, refers to both you and us. All terms refer to all considerations of Reward That Crew necessary to undertake support to you for the express purpose of meeting your user needs in respect of our services, under and subject to, prevailing law of the state or country in which Reward That Crew operates. Any use of these definitions or other glossary in the singular, plural, capitalization, and/or pronoun are interchangeable but refer to the same. </p>
                      <h5 class="fsbold">Intellectual Property Rights  </h5>
                      <p>Other than the content you own and opted to include on this website, under these terms Reward That Crew and/or its licensers own and reserve all intellectual property rights of this website. You are granted a limited license, subject to the restrictions entailed in the Terms and Conditions, for purposes of viewing this website’s content. </p>
                      <h5 class="fsbold">Tipping through Reward That Crew:   </h5>
                      <p>Reward That Crew will apply a processing fee of ten percent to the tips received through the website. Tips will be paid out the first week of the month for the previous month. All tips will be sent to the business that has subscribed to Reward That Crew. The subscriber is free to distribute the tip funds as they see appropriate. </p>
                </div>
                   
                 
                </div>

            </div>
        </div>
        <!--/ sub page body -->

     
      
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>