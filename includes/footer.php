<!-- footer -->
    <footer>
        <div class="container">
            <div class="row pb-5">
                <div class="col-md-4">
                    <h4 class="h4 fbold pb-2">Rate the Crew</h4>
                   <p>We have found that our system creates an atmosphere of friendly competition. The objective: to see who can impress the customers and supervisors the most! This, in turn, is greatly beneficial for your customers, your staff, and ultimately, your business <a href
                   ="about.php"><strong>Read More</strong></a></p>
                </div>
                <div class="col-md-4">
                    <h6 class="h6 fsbold">Company</h6>
                    <ul>
                        <li><a href="about.php">About us</a></li>
                        <li><a href="benefits.php">Benefits</a></li>
                        <li><a href="contact.php">Contact us</a></li>
                        <li><a href="terms.php">Terms &amp; conditions</a></li>
                        <li><a href="privacy.php">Privacy</a></li>
                    </ul>
                </div>  
                 <div class="col-md-4">
                    <h6 class="h6 fsbold">Social</h6>
                      <div class="footerSocial">
                        <a href="javascript:void(0)" target="_blank">
                            <span class="icon-facebook icomoon"></span>
                        </a>
                        <a href="javascript:void(0)" target="_blank">
                            <span class="icon-twitter icomoon"></span>
                        </a>
                        <a href="javascript:void(0)" target="_blank">
                            <span class="icon-linkedin icomoon"></span>
                        </a>
                    </div>
                    <p class="pt-4">© 2021 Rate that Crew. All Rights Resevered</p>
                    
                </div>                 
            </div>
            <div class="row justify-content-end">              
                
                <div class="col-md-4 text-end">
                    <a href="javascript:void(0)" id="movetop">^ Back to Top</a>
                </div>
            </div>
        </div>
    </footer>
    <!--/ footer -->