 <!-- header starts -->
<header class="fixed-top">
    <div class="topHeader">
        <div class="container">
            <div class="row py-md-2">
                <div class="col-md-6">
                    <p class="pb-0 mb-0">Phone: <b>+212.913.9058</b></p>
                </div>
                <div class="col-md-6 text-md-end">
                    <p class="pb-0 mb-0">Email: <b>info@rewardthatcrew.com</b></p>
                </div>
            </div>
        </div>
    </div>
    <div class="bottomHeader">
        <div class="container position-relative">
            <div class="navbar navbar-expand-lg bsnav bsnav-light">
                <a class="navbar-brand" href="index.php">
                    <img src="img/logo.png" alt="">
                </a>
                <button class="navbar-toggler toggler-spring"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse justify-content-between">
                    <ul class="navbar-nav navbar-mobile ps-3">
                        <li class="nav-item"><a class="active" href="index.php">Home</a></li>
                        <li class="nav-item"><a class="nav-link" href="about.php">About</a></li>
                        <li class="nav-item"><a class="nav-link" href="benefits.php">Benefits</a></li>
                        <li class="nav-item"><a class="nav-link" href="restaurants.php">Restaurants</a></li>
                        <li class="nav-item"><a class="nav-link" href="allcrews.php">Crews</a></li>
                    </ul>
                    <ul class="navbar-nav navbar-mobile">
                        <li class="nav-item">
                            <a class="nav-link loginlink" href="signin.php"><span class="icon-udrt"></span> Sign
                                in / Sign up</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="bsnav-mobile">
                <div class="bsnav-mobile-overlay"></div>
                <div class="navbar"></div>
            </div>
        </div>
    </div>
</header>
<!--/header ends -->