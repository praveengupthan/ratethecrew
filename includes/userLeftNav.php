<nav class="navbar navbar-expand-lg navbar-light userleftNav">
    <button class="navbar-togglerProfile" type="button" data-bs-toggle="collapse" data-bs-target="#ProfileNavPostLogin" aria-controls="navbarSupportedContent"   aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      Navigation
    </button>
    <div class="collapse navbar-collapse" id="ProfileNavPostLogin">
      <ul class="navbar-nav mb-2 mb-lg-0 leftUserNav">
        <li class="nav-item"><a class="active" href="userDashboard.php"><span>Dashboard</span></a></li>
       <li class="nav-item"><a class="nav-link" href="customerProfile.php"><span>My Profile</span></a></li>
        <li class="nav-item"><a class="nav-link" href="customerProfileEdit.php"><span>Edit Profile</span></a></li>
        <li class="nav-item"><a class="nav-link" href="#"><span>Stores</span></a></li>
        <li class="nav-item"><a class="nav-link" href="#"><span>My Crews</span></a></li>            
        <li class="nav-item"><a class="nav-link" href="#"><span>Rewards</span></a></li>
        <li class="nav-item"><a class="nav-link" href="#"><span>Feedback </span></a></li>
        <li class="nav-item"><a class="nav-link" href="#"><span>Internal Feedback</span></a></li>     
        <li class="nav-item"><a class="nav-link" href="customernotifications.php"><span>Notifications</span></a></li>      
        <li class="nav-item"><a class="nav-link" href="#"><span>Invoices</span></a></li>       
      </ul>     
    </div>

</nav>