<!-- style sheets -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bsnav.min.css">
<link rel="stylesheet" href="css/icomoon.css">
<link rel="stylesheet" href="css/easy-responsive-tabs.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.0/font/bootstrap-icons.css">
<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/quill.css">
<link rel="stylesheet" href="css/quill.snow.css">
<link rel="stylesheet" href="css/quill.bubble.css">
<link rel="stylesheet" href="css/style.css">