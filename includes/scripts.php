<!-- scripts -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bsnav.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <!--[if lte IE 9]>
            <script src="js/ie.lteIE9.js"></script>
        <![endif]-->
    <script src="js/easyResponsiveTabs.js"></script>
    <script src="js/custom.js"></script>
  
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="js/bootstrap-quill.js"></script>
  <script src="js/sprite.svg.js"></script>
    <!-- scripts -->
    <!-- jquery form validation-->
    <script>
        //contact form validatin
        $('#contact_form').validate({
            ignore: [],
            errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            errorPlacement: function (error, e) {
                e.parents('.form-group').append(error);
            },
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.text-danger').remove();
            },

            rules: {
                name: {
                    required: true
                },
                phone: {
                    required: true,
                    number: true,
                    minlength: 10,
                    maxlength: 10
                },
                email: {
                    required: true,
                    email: true
                }

            },

            messages: {
                name: {
                    required: "Enter Name"
                },
                phone: {
                    required: "Enter Valid Mobile Number (do not include +91)"
                },
                email: {
                    required: "Enter Valid Email"
                },
            },
        });

    </script>
 <script>
    var toolbarOptions = [
      [{
        'header': [1, 2, 3, 4, 5, 6, false]
      }],
      ['bold', 'italic', 'underline', 'strike'], // toggled buttons
      ['blockquote', 'code-block'],

      [{
        'header': 1
      }, {
        'header': 2
      }], // custom button values
      [{
        'list': 'ordered'
      }, {
        'list': 'bullet'
      }],
      [{
        'script': 'sub'
      }, {
        'script': 'super'
      }], // superscript/subscript
      [{
        'indent': '-1'
      }, {
        'indent': '+1'
      }], // outdent/indent
      [{
        'direction': 'rtl'
      }], // text direction

      [{
        'size': ['small', false, 'large', 'huge']
      }], // custom dropdown

      [{
        'color': []
      }, {
        'background': []
      }], // dropdown with defaults from theme
      [{
        'font': []
      }],
      [{
        'align': []
      }],
      ['link', 'image'],

      ['clean'] // remove formatting button
    ];

    var quillFull = new Quill('#document-full', {
      modules: {
        toolbar: toolbarOptions,
        autoformat: true
      },
      theme: 'snow',
      placeholder: "Write something..."
    });

    var quillCompact = new Quill('#document-compact', {
      modules: {
        form: {
          submitKey: {
            key: 'Enter',
            shortKey: true
          }
        },
        autoformat: true
      },
      theme: 'bubble',
      placeholder: "Type a message. " + (/mac/i.test(navigator.platform) ? "⌘" : "Ctrl") + "+Enter to send."
    });

  </script>