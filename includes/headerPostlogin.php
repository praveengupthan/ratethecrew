 <!-- header starts -->
    <header class="fixed-top headerPostlogin">
        <div class="container position-relative">
            <div class="navbar navbar-expand-lg bsnav bsnav-light">
                <a class="navbar-brand" href="index.php">
                    <img src="img/logo.png" alt="">
                </a>
                <button class="navbar-toggler toggler-spring"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse justify-content-between">
                    <ul class="navbar-nav navbar-mobile ps-3">
                        <li class="nav-item"><a class="active" href="index.php">Home</a></li>
                        <li class="nav-item"><a class="nav-link" href="about.php">About</a></li>
                        <li class="nav-item"><a class="nav-link" href="benefits.php">Benefits</a></li>
                        <li class="nav-item"><a class="nav-link" href="restaurants.php">Restaurants</a></li>
                        <li class="nav-item"><a class="nav-link" href="allcrews.php">Crews</a></li>
                    </ul>
                    <ul class="navbar-nav navbar-mobile">
                        <li class="nav-item dropdown fadeup">
                            <a class="nav-link" href="#">
                                <span>Username will be here</span>
                                <img src="img/crews/crew01.jpg" alt="" class="thumbimg">
                            </a>
                            <ul class="navbar-nav">
                                <li class="nav-item"><a class="nav-link" href="#">Profile</a></li>                                
                                <li class="nav-item"><a class="nav-link" href="index.php">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="bsnav-mobile">
                <div class="bsnav-mobile-overlay"></div>
                <div class="navbar"></div>
            </div>
        </div>
    </header>
    <!--/header ends -->