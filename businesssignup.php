<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rate the Crew</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
     <?php include 'includes/styles.php'?>
     <?php include 'includes/arrayObjects.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main class="subPage">       
        <!-- sub page body -->
        <div class="subpageBody registerbody">
            <div class="container">

                <!-- card -->
                <div class="card formCard">
                    <h5 class="card-title text-uppercase">Business Details</h5>
                    <div class="card-body">
                        <div class="row">
                            <!-- col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Business/Store Name *</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Write your Business Name">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name of Brand *</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Name of Brand">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Upload Logo *</label>
                                    <div class="input-group">
                                        <input type="file" class="form-control" placeholder="Upload logo">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Office Phone Number *</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Office Phone Number">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name of Business Owner *</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Write Name of Business Owner ">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Business Owner Phone Number *</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Business Phone Number">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->
                        </div>
                    </div>
                </div>
                <!--/ card -->

                 <!-- card -->
                <div class="card formCard">
                    <h5 class="card-title text-uppercase">Address Details</h5>
                    <div class="card-body">
                        <div class="row">
                            <!-- col -->
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Address*</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Address">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Area / City *</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Area">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>City *</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Write City">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>State *</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Write State">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Zip Code *</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Zip Code">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->
                        </div>
                    </div>
                </div>
                <!--/ card -->

                 <!-- card -->
                <div class="card formCard">
                    <h5 class="card-title text-uppercase">Login Details</h5>
                    <div class="card-body">
                        <div class="row">                          

                            <!-- col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Email Address *</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Email Address">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Password *</label>
                                    <div class="input-group">
                                        <input type="password" class="form-control" placeholder="Enter Password">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Confirm Password *</label>
                                    <div class="input-group">
                                        <input type="password" class="form-control" placeholder="Confirm Password">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->
                        </div>
                    </div>
                </div>
                <!--/ card -->

                <button class="redlink">Signup </button>


            </div>
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>