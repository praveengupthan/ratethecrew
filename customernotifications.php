<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rate the Crew</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
     <?php include 'includes/styles.php'?>
     <?php include 'includes/arrayObjects.php'?>
</head>

<body>
    <?php include 'includes/headerPostlogin.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
       <div class="container profilePage">
           <h2 class="h4 fbold pb-3">Notifications</h2>

           <div class="d-sm-flex justify-content-between">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="customerProfile.php">Username</a></li>
                        <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
                        <li class="breadcrumb-item active" aria-current="page">Notifications</li>
                    </ol>
                </nav>
                <p class="fgray text-right">Last Updated on : <span class="fblack fsbold">31 May 2021</span></p>
           </div>
            
       </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpageBody postLoginPage">
             <!-- container -->
            <div class="container">  
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-md-2">
                       <?Php include 'includes/userLeftNav.php'?>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-10">
                        <!-- right profile -->
                        <div class="rightProfile">
                            <!-- row-->
                            <div class="row">
                                <!-- left items -->
                                <div class="col-lg-3">
                                <a href="customernotifications-compose.php" type="button" class="btn btn-primary d-block w-100 mb-2"><i class="bi bi-envelope"></i> Compose</a>
                                    <ul class="p-3 border">
                                        <li>
                                            <a href="javascript:void(0)"><i class="bi bi-journal-bookmark-fill"></i> Unread Messages (32)</a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)"><i class="bi bi-envelope"></i> Messages (32)</a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)"><i class="bi bi-megaphone"></i> Announcements</a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)"><i class="bi bi-send"></i> Sent</a>
                                        </li>                                        
                                    </ul>
                                </div>
                                <!--/ left items -->
                                <!-- right items -->
                                <div class="col-lg-9 notifications">
                                    <!-- header -->
                                    <div class="notificationsHeader p-3 border mb-2">
                                        <duv class="row">
                                            <div class="col-md-6 align-self-center">
                                                <a href="javascript:void(0)"><i class="bi bi-trash"></i> Delete</a>
                                            </div>
                                            <div class="col-md-6 text-end">
                                                <nav aria-label="Page navigation example">
                                                    <ul class="pagination justify-content-end">
                                                        <li class="page-item disabled">
                                                        <a class="page-link" href="#" tabindex="-1">Previous</a>
                                                        </li>
                                                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                        <li class="page-item">
                                                        <a class="page-link" href="#">Next</a>
                                                        </li>
                                                    </ul>
                                                </nav>
                                            </div>
                                        </duv>
                                    </div>
                                    <!--/ header -->
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            <div class="row">
                                                <div class="col-md-1 text-center">
                                                    <a href="javascript:void(0)"><i class="bi bi-star"></i></a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a href="javascript:void(0)">1089018320</a>
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="javascript:void(0)">My Kids Lost Items</a>
                                                </div>
                                                <div class="col-md-4 text-end">
                                                    <small class="text-tray">8th, Apr 2022, 01:35PM</small>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="row">
                                                <div class="col-md-1 text-center">
                                                    <a href="javascript:void(0)"><i class="bi bi-star active"></i></a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a href="javascript:void(0)">1089018320</a>
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="javascript:void(0)">My Kids Lost Items</a>
                                                </div>
                                                <div class="col-md-4 text-end">
                                                    <small class="text-tray">8th, Apr 2022, 01:35PM</small>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="row">
                                                <div class="col-md-1 text-center">
                                                    <a href="javascript:void(0)"><i class="bi bi-star"></i></a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a href="javascript:void(0)">1089018320</a>
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="javascript:void(0)">My Kids Lost Items</a>
                                                </div>
                                                <div class="col-md-4 text-end">
                                                    <small class="text-tray">8th, Apr 2022, 01:35PM</small>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="row">
                                                <div class="col-md-1 text-center">
                                                    <a href="javascript:void(0)"><i class="bi bi-star"></i></a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a href="javascript:void(0)">1089018320</a>
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="javascript:void(0)">My Kids Lost Items</a>
                                                </div>
                                                <div class="col-md-4 text-end">
                                                    <small class="text-tray">8th, Apr 2022, 01:35PM</small>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="row">
                                                <div class="col-md-1 text-center">
                                                    <a href="javascript:void(0)"><i class="bi bi-star"></i></a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a href="javascript:void(0)">1089018320</a>
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="javascript:void(0)">My Kids Lost Items</a>
                                                </div>
                                                <div class="col-md-4 text-end">
                                                    <small class="text-tray">8th, Apr 2022, 01:35PM</small>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="row">
                                                <div class="col-md-1 text-center">
                                                    <a href="javascript:void(0)"><i class="bi bi-star"></i></a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a href="javascript:void(0)">1089018320</a>
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="javascript:void(0)">My Kids Lost Items</a>
                                                </div>
                                                <div class="col-md-4 text-end">
                                                    <small class="text-tray">8th, Apr 2022, 01:35PM</small>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="row">
                                                <div class="col-md-1 text-center">
                                                    <a href="javascript:void(0)"><i class="bi bi-star"></i></a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a href="javascript:void(0)">1089018320</a>
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="javascript:void(0)">My Kids Lost Items</a>
                                                </div>
                                                <div class="col-md-4 text-end">
                                                    <small class="text-tray">8th, Apr 2022, 01:35PM</small>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <!--/ right items -->
                            </div>
                            <!--/ row -->
                           
                        </div>
                        <!--/ right profile -->
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
              
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->

     
      
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>