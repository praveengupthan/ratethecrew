<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rate the Crew</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
     <?php include 'includes/styles.php'?>
     <?php include 'includes/arrayObjects.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
       <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
                    <li class="breadcrumb-item active" aria-current="page">Crews</li>
                </ol>
            </nav>
       </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpageBody">
             <!-- container -->
            <div class="container topRatedcrews">             
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <?php 
                    for($i=0; $i<count ($crewItem); $i++){?>
                    <div class="col-md-4 col-lg-3 text-center crewcol">
                        <figure>
                            <a href="javascript:void(0)">
                                <img src="img/crews/<?php echo $crewItem [$i][0]?>.jpg" alt="" class="img-fluid">
                            </a>
                        </figure>
                        <article>
                            <a class="fbold h5" href="javascript:void(0)"><?php echo $crewItem [$i][1]?></a>
                            <p><small><?php echo $crewItem [$i][2]?></small></p>
                            <p class="border-top pt-3"><span class="customerRate px-2 white me-2"><?php echo $crewItem [$i][3]?></span>Customer
                                Rating </p>
                        </article>
                    </div>
                    <?php } ?>
                    <!--/ col -->              
                </div>
                <!--/ row -->
              
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->

     
      
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>