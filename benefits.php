<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rate the Crew</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
     <?php include 'includes/styles.php'?>
     <?php include 'includes/arrayObjects.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
       <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
                    <li class="breadcrumb-item active" aria-current="page">Benefits</li>
                </ol>
            </nav>
       </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpageBody">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 align-self-center">
                        <h2>Benefits of participating:</h2>
                        <ul>
                            <li>Increased employee satisfaction (after all, who doesn’t want free money!?)</li>
                            <li>Increased customer satisfaction (employees work harder to secure nominations)</li>
                            <li>Improved employee retention</li>
                            <li>Creates an atmosphere of positive reinforcement</li>
                            <li>Creates the ability to receive tips. </li>
                        </ul>
                        <p>To sign up for Reward that Crew or for any questions email <a href="emailto:"rewardthatcrew@gmail.com" class="fred">rewardthatcrew@gmail.com</a></p>
                    </div>
                    <div class="col-md-6">
                        <img src="img/about1.jpg" alt="" class="img-fluid w-100">
                    </div>
                </div>

                 <div class="row py-5 d-none">
                    <div class="col-md-6 order-lg-last align-self-center">
                        <h2>Benefits for Crew</h2>
                        
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et.    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et.  </p>

                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et.  </p>
                       
                    </div>
                    <div class="col-md-6">                        
                        <img src="img/benefitsimg.jpg" alt="" class="img-fluid w-100">
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page body -->

     
      
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>