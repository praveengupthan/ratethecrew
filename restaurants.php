<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rate the Crew</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
     <?php include 'includes/styles.php'?>
     <?php include 'includes/arrayObjects.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
       <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
                    <li class="breadcrumb-item active" aria-current="page">Restaurants</li>
                </ol>
            </nav>
       </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpageBody">
             <!-- container -->
            <div class="container">
               <div class="d-flex justify-content-between">
                    <h2 class="sectionTitle"> Top Rated Restaurants </h2>                   
               </div>
                <!-- tab starts -->
                <div class="TopRestaurantsTab">
                    <div id="parentHorizontalTab">
                        <ul class="resp-tabs-list hor_1">
                            <li>All</li>
                            <li>Featured</li>
                            <li>Best Rate</li>
                            <li>Most View</li>
                            <li>Popular</li>
                        </ul>
                        <div class="resp-tabs-container hor_1">
                            <div>
                                <!-- list -->
                                <div class="listRest">
                                    <div class="row">
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest01img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Raman Kraft Hotel</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam  nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest03img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Flavor Town</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest02img.jpg" class="img-fluid" alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Big Bites</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest01img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Choice Foods</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                    </div>
                                </div>
                                <!-- list -->
                            </div>
                            <div>
                                <!-- features -->
                                <!-- list -->
                                <div class="listRest">
                                    <div class="row">
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest01img.jpg" class="img-fluid" alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Raman Kraft Hotel</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest02img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Flavor Town</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest03img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Big Bites</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest04img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Choice Foods</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                    </div>
                                </div>
                                <!-- list -->
                                <!--/ featured-->
                            </div>
                            <div>
                                <!-- best rate-->
                                <!-- list -->
                                <div class="listRest">
                                    <div class="row">
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest04img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Raman Kraft Hotel</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest03img.jpg" class="img-fluid" alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Flavor Town</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest02img.jpg" class="img-fluid" alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Big Bites</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest01img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Choice Foods</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco, CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                    </div>
                                </div>
                                <!-- list -->
                                <!--/ best rate-->
                            </div>
                            <div>
                                <!--  Most view -->
                                <!-- list -->
                                <div class="listRest">
                                    <div class="row">
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest01img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Raman Kraft Hotel</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest02img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Flavor Town</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest03img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Big Bites</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest04img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Choice Foods</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                    </div>
                                </div>
                                <!-- list -->
                                <!--/ Most view -->
                            </div>
                            <div>
                                <!-- popular -->
                                <!-- list -->
                                <div class="listRest">
                                    <div class="row">
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest04img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Raman Kraft Hotel</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest03img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Flavor Town</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest02img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Big Bites</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                        <!--item -->
                                        <div class="col-md-4 col-lg-3">
                                            <div class="restItem">
                                                <div class="restItemfigure">
                                                    <a href="javascript:void(0)">
                                                        <img src="img/restaurantImg/rest01img.jpg" class="img-fluid"
                                                            alt="">
                                                    </a>
                                                    <span class="label position-absolute py-1 px-2 white"> Top Rated
                                                    </span>
                                                </div>
                                                <article>
                                                    <h3>
                                                        <a href="javascript:void(0)">Choice Foods</a>
                                                    </h3>
                                                    <small><span class="icon-location icomoon"></span>San Francisco,
                                                        CA</small>
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                        erat, sed diam.</p>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ item -->
                                    </div>
                                </div>
                                <!-- list -->
                                <!--/ popular -->
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ tab ends -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->

     
      
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>