<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rate the Crew</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
     <?php include 'includes/styles.php'?>
     <?php include 'includes/arrayObjects.php'?>
</head>

<body>
    <?php include 'includes/headerPostlogin.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
       <div class="container profilePage">
           <h2 class="h4 fbold pb-3">My Profile</h2>

           <div class="d-sm-flex justify-content-between">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="customerProfile.php">Username</a></li>
                        <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
                        <li class="breadcrumb-item active" aria-current="page">Profile</li>
                    </ol>
                </nav>
                <p class="fgray text-right">Last Updated on : <span class="fblack fsbold">31 May 2021</span></p>
           </div>
            
       </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpageBody postLoginPage">
             <!-- container -->
            <div class="container">  
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-md-2">
                       <?Php include 'includes/userLeftNav.php'?>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-10">
                        <!-- right profile -->
                        <div class="rightProfile">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 class="h4 fsbold py-3">Table Title Here</h3>
                                </div>
                                <div class="col-md-6 text-end">
                                    <a href="javascript:void(0)" class="redlink" data-bs-toggle="modal" data-bs-target="#exampleModal">Add / Edit</a>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">First</th>
                                            <th scope="col">Last</th>
                                            <th scope="col">Handle</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>Mark</td>
                                            <td>Otto</td>
                                            <td>@mdo</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">2</th>
                                            <td>Jacob</td>
                                            <td>Thornton</td>
                                            <td>@fat</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">3</th>
                                            <td>Larry</td>
                                            <td>the Bird</td>
                                            <td>@twitter</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--/ right profile -->
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
              
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->    
      
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
          <!-- card -->
                <div class="card formCard">
                    <h5 class="card-title text-uppercase">Customer Basic Details</h5>
                    <div class="card-body">
                        <div class="row">
                            <!-- col -->
                            <div class="col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label>First Name *</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="First Name">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label>Middle Name *</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Middle Name">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label>Last Name *</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Last Name">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                            <div class="col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label>Phone Number *</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Phone Number">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->                         

                             <!-- col -->
                            <div class="col-md-6 col-lg-8">
                                <div class="form-group">
                                    <label>Address *</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Address">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                              <!-- col -->
                            <div class="col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label>City *</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Write City">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label>State *</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Write State">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                            <div class="col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label>Zip Code *</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Zip Code">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                            <div class="col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label>Favourite Food1 *</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Favourite Food1">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                            <div class="col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label>Favourite Food2 *</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Favourite Food2">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                            <div class="col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label>Favourite Food3 *</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Favourite Food3">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->
                              <!-- col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Upload the Document *</label>
                                    <div class="input-group">
                                        <input type="file" class="form-control" placeholder="Upload the file">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Select State *</label>
                                    <div class="input-group">
                                       <select class="form-control">
                                           <option>Telangana</option>
                                           <option>Andhra Pradesh</option>
                                           <option>Odisha</option>
                                           <option>Tamilnadu</option>
                                           <option>Karnataka</option>
                                       </select>
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                            <div class="col-md-4">
                                <div class="form-group">       
                                    <label class="d-block">Select Gender *</label>                             
                                   <div class="form-check form-check-inline mt-2">
                                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
                                        <label class="form-check-label" for="flexRadioDefault1">
                                           Male
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline mt-2">
                                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked>
                                        <label class="form-check-label" for="flexRadioDefault2">
                                           Female
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Select Date *</label>
                                    <div class="input-group">
                                        <input type="date" class="form-control" placeholder="Select Date">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Write Description *</label>
                                    <div class="input-group">
                                        <textarea class="form-control" style="height:120px;" placeholder="Write Description"></textarea>
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->
                        </div>
                    </div>
                </div>
                <!--/ card -->   
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
</body>

</html>