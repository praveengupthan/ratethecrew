<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rate the Crew</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
     <?php include 'includes/styles.php'?>
     <?php include 'includes/arrayObjects.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
       <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
                    <li class="breadcrumb-item active" aria-current="page">Contact & Support</li>
                </ol>
            </nav>
       </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpageBody">
            <div class="container">
                <div class="row">

                   <div class="col-md-6">
                       <article class="pb-4">
                           <h4 class="h4">Choice Screening strives to provide the best service possible with every contact! </h4>
                           <p>We operate in an industry built on trust. This can only be achieved through communication and experienced support – from the first contact past your ten-year anniversary.</p>
                       </article>

                        <article class="pb-4">
                           <h4 class="h4">At Choice you always talk to a human! </h4>
                           <p>Have questions about background screening? Our entire team receives specialized training regularly to ensure you're receiving the best information possible. From basic questions to complex compliance inquiries, we're here to help!</p>
                           <p>Interested in learning more about our services? Our Account Executives take the time to discuss your existing background screening program and help you make smart decisions that best meet your needs. </p>
                       </article>
                   </div>

                   <div class="col-md-1 seperatorCol"></div>

                   <div class="col-md-3">
                       <article>
                           <h4 class="h4">Corporate Office </h4>
                           <p>8668 Concord Center Dr.Englewood, CO 80112</p>
                       </article>

                       <article>
                           <h4 class="h4">Direct Contact</h4>
                           <p class="pb-0 mb-0">Phone: <b>720.974.7878</b></p>
                           <p class="pb-0 mb-0">Toll Free: <b>1.877.929.7878</b></p>
                           <p>Email: <b>info@choicescreening.com </b></p>
                       </article>

                        <article>
                           <h4 class="h4">Departments</h4>
                           <p class="pb-0 mb-0">customerservice@rewardthatcrew.com</p>
                           <p class="pb-0 mb-0">sales@rewardthatcrew.com</p>
                           <p class="pb-0 mb-0">disputes@rewardthatcrew.com</p>
                           <p class="pb-0 mb-0">compliance@rewardthatcrew.com</p>
                           <p class="pb-0 mb-0">careers@rewardthatcrew.com</p>                           
                       </article>
                   </div>
                </div>
            </div>

            <!-- contact form -->
            <div class="formcontact">
                <div class="container">
                    <h4 class="py-4">Contact us Today</h4>
                    <form>
                        <div class="row">
                            <!-- col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>First Name *</label>
                                    <div class="input-group">                                       
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <div class="input-group">                                       
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Job Title *</label>
                                    <div class="input-group">                                       
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Phone Number *</label>
                                    <div class="input-group">                                       
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Email*</label>
                                    <div class="input-group">                                       
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>State *</label>
                                    <div class="input-group">                                       
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>How did you hear about us</label>
                                    <div class="input-group">                                       
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Write Message</label>
                                    <div class="input-group">                                       
                                        <div class="input-group">
                                            <textarea class="form-control" style="height:85px;"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->

                            <div class="col-md-4">
                                <button class="redlink d-block">SUBMIT INFORMATION</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--/ contact form -->
        </div>
        <!--/ sub page body -->

     
      
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>