<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rate the Crew</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
    <?php include 'includes/styles.php'?>
    <?php include 'includes/arrayObjects.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
       <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
                    <li class="breadcrumb-item active" aria-current="page">Privacy Policy</li>
                </ol>
            </nav>
       </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpageBody">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2>Privacy Policy</h2>
                        <p>Reward That Crew operates rerwardthatcrew.com, which provides a service. </p>
                        <p>This page is used to inform website visitors and users regarding our policies with the collection and use of information in relation with this policy. The personal information that we collect may be used for providing and improving the services. We will not use or share your information with anyone except as described in the Privacy Policy. </p>
                        <h5 class="fsbold">Information Collection and Use:</h5>
                        <p>For a better experience while using our service, we may require you to provide us with certain personally identifiable information, including but not limited to your name, phone number, and postal address. The information that we collect will be used to contact or identify you. </p>
                    </div>
                    <div class="col-md-6">
                        <h5 class="fsbold">Log Data: </h5>
                        <p>We want to inform you that whenever you visit our service, we collect information that your browser sends to us that is called log data. This log data may include information such as your computers internet Protocol (“IP”) address, browser version, pages of our service that you visit, the time and date of your visit, the time spent on those pages and other statistics.  </p>
                        <h5 class="fsbold">Cookies: </h5>
                        <p>Cookies are files with a small amount of data that is commonly used an anonymous unique identifier. These are sent to your browser from websites that you visit and are stored on your computers hard drive. </p>
                        <p>Our website uses these “cookies” to collect information and to improve our service. You have the option to either accept or refuse these cookies. If you refuse to accept these cookies you may not be able to use some or all portions of our service.  </p>
                    </div>   
                </div>
            </div>
        </div>
        <!--/ sub page body -->    
      
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>