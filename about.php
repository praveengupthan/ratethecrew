<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rate the Crew</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
     <?php include 'includes/styles.php'?>
     <?php include 'includes/arrayObjects.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
       <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
                    <li class="breadcrumb-item active" aria-current="page">About</li>
                </ol>
            </nav>
       </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpageBody">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 align-self-center">
                        <h2>Reward That Crew!</h2>
                        <h6>We are a company consumed with making life at work more enjoyable. </h6>
                        <p>We have found that our system creates an atmosphere of friendly competition.  The objective:  to see who can impress the customers and supervisors the most! This, in turn, is greatly beneficial for your customers, your staff, and ultimately, your business. </p>
                    </div>
                    <div class="col-md-6">
                        <img src="img/about1.jpg" alt="" class="img-fluid w-100">
                    </div>
                </div>

                <!-- row -->
                <div class="row">
                    <div class="col-md-12">
                        <h2>How it works: </h2>
                    </div>
                    <div class="col-md-6">
                        <h6>Monthly: </h6>
                        <p><i>Reward That Crew</i> allows both customers and supervisors to nominate any employee for doing a great job. The online nomination website is very easy to use. Each morning an updated e-mail is sent to the subscriber (you) and the electronic leader board shows all the votes.  At the end of the month you can use the information as you see fit.  </p>
                        <p>This program has been highly successful for businesses who have participated. A common option is for the employee with the most votes in the organization to receive a $150 cash prize, with an additional cash prize attached to a random drawing (this ability is built into our website)**.</p>
                        <p>**The subscriber is responsible for all monthly rewards</p>
                    </div>
                     <div class="col-md-6">
                        <h6>Quarterly: </h6>
                        <p><i>Reward That Crew </i>securely stores every nomination received and automatically enters all nominated employees into a random quarterly drawing, which will be paid out by us. This cash prize will be delivered to the subscriber, who is responsible for giving it to the employee.  If the winning individual is no longer employed, the prize money is yours to do as you see fit. </p>
                    </div>
                </div>
                <!--/ row -->

                 <div class="row py-5">
                    <div class="col-md-6 order-lg-last align-self-center">
                        <h2>Why participate?:</h2>
                        <ul>
                            <li>Increased employee satisfaction (after all, who doesn’t want free money!?)</li>
                            <li>Increased customer satisfaction (employees work harder to secure nominations)</li>
                            <li>Improved employee retention</li>
                            <li>Creates an atmosphere of positive reinforcement</li>
                        </ul>
                        <p>To sign up for Reward that Crew or for any questions email <a href="mailto:rewardthatcrew@gmail.com">rewardthatcrew@gmail.com</a></p>                       
                    </div>
                    <div class="col-md-6">                        
                        <img src="img/about3.jpg" alt="" class="img-fluid w-100">
                    </div>
                </div>

                <div class="row justify-content-center d-none">
                    <div class="col-md-8 text-center">
                        <h2>Solving One Big Problem</h2>
                        <p>In 2009, three friends—Marco, Jonathan, and Sander—noticed a problem. Customers needed help connecting to qualified, local professionals. The same professionals needed customers. As a solution to bring the two together, TheDir was born. Since then, we’ve helped millions of customers with their projects and generated more than $1 billion in annual revenue for our pros. We’re reshaping local economies. We’re changing lives. And true to the spirit of our customers and pros, we’re getting things done.</p>
                    </div>
                </div>

                <div class="row justify-content-center py-5 d-none">
                    <div class="col-6 col-md-3 text-center">
                        <h2 class="h2 pb-0 mb-0 fred">80</h2>
                        <p class="pt-0 fblack">Countries</p>
                    </div>

                     <div class="col-6 col-md-3 text-center">
                        <h2 class="h2 pb-0 mb-0 fred">+ 3,000</h2>
                        <p class="pt-0 fblack">Cities</p>
                    </div>

                     <div class="col-6 col-md-3 text-center">
                        <h2 class="h2 pb-0 mb-0 fred">$4,650,729</h2>
                        <p class="pt-0 fblack">Business</p>
                    </div>

                     <div class="col-6 col-md-3 text-center">
                        <h2 class="h2 pb-0 mb-0 fred">9,452,785 + </h2>
                        <p class="pt-0 fblack">Users</p>
                    </div>

                </div>


            </div>
        </div>
        <!--/ sub page body -->

     
      
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>