<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rate the Crew</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
     <?php include 'includes/styles.php'?>
     <?php include 'includes/arrayObjects.php'?>
</head>

<body>
    <?php include 'includes/headerPostlogin.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
       <div class="container profilePage">
           <h2 class="h4 fbold pb-3"> Restaurant Dashboard</h2>

           <div class="d-sm-flex justify-content-between">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="customerProfile.php">Username</a></li>
                        <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
                        <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                    </ol>
                </nav>
                <p class="fgray text-right">Last Updated on : <span class="fblack fsbold">31 May 2021</span></p>
           </div>
            
       </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpageBody postLoginPage">
             <!-- container -->
            <div class="container">  
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-md-2">
                       <?Php include 'includes/userLeftNav.php'?>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-10">
                        <!-- right profile -->
                        <div class="rightProfile">
                            <!-- row -->
                            <div class="row rowdb">
                                <!-- col -->
                                <div class="col-md-4">
                                    <div class="dbCol">
                                        <h3 class="fsbold h4 d-block border-bottom pb-2 mb-2">Stores</h3>
                                        <h2 class="h4 d-block text-end pt-3">1</h2>
                                    </div>
                                </div>
                                <!--/ col -->
                                 <!-- col -->
                                <div class="col-md-4">
                                    <div class="dbCol">
                                        <h3 class="fsbold h4 d-block border-bottom pb-2 mb-2">Crews</h3>
                                        <h2 class="h4 d-block text-end pt-3">2</h2>
                                    </div>
                                </div>
                                <!--/ col -->
                                 <!-- col -->
                                <div class="col-md-4">
                                    <div class="dbCol">
                                        <h3 class="fsbold h4 d-block border-bottom pb-2 mb-2">Invoices</h3>
                                        <h2 class="h4 d-block text-end pt-3">0</h2>
                                    </div>
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ row -->

                            <h4 class="pt-4 fsbold">Current Reward Program</h4>

                            <!-- table row -->
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table">                                       
                                        <tbody>
                                           <tr>
                                               <td>Name of Reward</td>
                                               <td>:</td>
                                               <td>Testing Promo</td>
                                           </tr>
                                            <tr>
                                               <td>Month</td>
                                               <td>:</td>
                                               <td>September</td>
                                           </tr>
                                            <tr>
                                               <td>Prize Money</td>
                                               <td>:</td>
                                               <td>10000.00</td>
                                           </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!--/ table row -->
                        </div>
                        <!--/ right profile -->
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
              
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->    
      
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>