<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rate the Crew</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
     <?php include 'includes/styles.php'?>
     <?php include 'includes/arrayObjects.php'?>
</head>

<body>
    <?php include 'includes/headerPostlogin.php'?>
    <!-- main -->
    <main class="subPage">
        <!-- sub page header -->
       <div class="container profilePage">
           <h2 class="h4 fbold pb-3">My Profile</h2>

           <div class="d-sm-flex justify-content-between">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="customerProfile.php">Username</a></li>                        
                        <li class="breadcrumb-item active" aria-current="page">Profile</li>
                    </ol>
                </nav>
                <p class="fgray text-right">Last Updated on : <span class="fblack fsbold">31 May 2021</span></p>
           </div>
            
       </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpageBody postLoginPage">
             <!-- container -->
            <div class="container">  
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-md-2">
                        <?Php include 'includes/userLeftNav.php'?>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-10">
                        <!-- right profile -->
                        <div class="rightProfile">
                            <!-- row -->
                            <div class="row justify-content-center userProfilePic">
                                <div class="col-md-6 text-center">
                                     <img src="img/crews/crew01.jpg" alt="" class="thumbimg">
                                     <h5 class="pt-3 mb-0 pb-0">Praveen Kumar Nandipati</h5>
                                     <p><small class="fsbold fgray">New Jersey, United States of America</small></p>
                                     <p>
                                         <span>Mob  :   +91 9642123254</span>
                                         <span class="d-inline-block ps-3">Email   :   praveennandipati@gmail.com</span>
                                      </p>
                                </div>
                            </div>
                            <!--/ row -->

                            <!-- row -->
                            <div class="row pt-3">
                                <div class="col-md-4">
                                    <div class="col-item">
                                        <h6 class="fsbold fgray">Date of Birth</h6>
                                        <p>4th August 1981</p>
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                    <div class="col-item">
                                        <h6 class="fsbold fgray">Gender</h6>
                                        <p>Male</p>
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                    <div class="col-item">
                                        <h6 class="fsbold fgray">Country</h6>
                                        <p>United States</p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-item">
                                        <h6 class="fsbold fgray">Current State</h6>
                                        <p>New Jersey</p>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="col-item">
                                        <h6 class="fsbold fgray">Permanent Address</h6>
                                        <p>141 Grant St, Ridgewood, New Jersey(NJ), 07450</p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-item">
                                        <h6 class="fsbold fgray">Favourite Food 1</h6>
                                        <p>Food Name will be here</p>
                                    </div>
                                </div>
                                  <div class="col-md-4">
                                    <div class="col-item">
                                        <h6 class="fsbold fgray">Favourite Food 1</h6>
                                        <p>Food Name will be here</p>
                                    </div>
                                </div>
                                  <div class="col-md-4">
                                    <div class="col-item">
                                        <h6 class="fsbold fgray">Favourite Food 1</h6>
                                        <p>Food Name will be here</p>
                                    </div>
                                </div>
                            </div>
                            <!--/ row -->
                        </div>
                        <!--/ right profile -->
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
              
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->  
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>