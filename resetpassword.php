<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rate the Crew</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
     <?php include 'includes/styles.php'?>
     <?php include 'includes/arrayObjects.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>
    <!-- main -->
    <main class="subPage">       
        <!-- sub page body -->
        <div class="subpageBody">
            <div class="container">
                <div class="row justify-content-center">

                <div class="col-md-5">
                    <div class="signinCol">
                        <article>
                            <h3 class="fbold">Reset Password</h3>
                            <p>Type the email you use to login to 7shifts and we'll send you instructions on how to create a shiny new password.</p>
                        </article>

                        <form class="form pt-3" method="">
                            <div class="form-group">
                                <label>Registered Email Address</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="">
                                </div>
                            </div>

                            <button class="redlink mt-3 d-block w-100">Send Password to Reset</button>
                            <p class="text-center py-2">
                                <a href="signin.php" class="fblack">Back to Signin</a>
                            </p>                         
                        </form>
                    </div>
                </div>  
                
                

                  
                </div>
            </div>
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
</body>

</html>

